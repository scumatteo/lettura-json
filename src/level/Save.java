package level;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Save {
	
	
	public void save(Tank player, Tank enemy) { 
		JSONObject obj = new JSONObject();
    JSONArray p = new JSONArray();  
    p.add(player.getPosX());  
    p.add(player.getPosY()); 
    p.add(player.getWidth());
    p.add(player.getHeight());
    JSONArray e= new JSONArray();  
    e.add(enemy.getPosX());  
    e.add(enemy.getPosY()); 
    e.add(enemy.getWidth());
    e.add(enemy.getHeight());
   obj.put(player.getName(), p);
   obj.putIfAbsent(enemy.getName(), e);

    try {  

        // Writing to a file  
        File file=new File("resources/level/save.json");  
        file.createNewFile();  
        FileWriter fileWriter = new FileWriter(file);  
        System.out.println("Writing JSON object to file");  
        System.out.println("-----------------------");  
        System.out.print(obj);  

        fileWriter.write(obj.toJSONString());  
        fileWriter.flush();  
        fileWriter.close();  

    } catch (IOException s) {  
        s.printStackTrace();  
    }  
	}

}
