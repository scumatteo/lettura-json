package level;

import javafx.geometry.Rectangle2D;

public class Tank {
	private String name;
	private double posX;
	private double posY;
	private double width;
	private double height;
	
	

	public Tank(String name) {
		super();
		this.name = name;

	}
	
	public void setTank(double posX, double posY, double width, double height) {
		this.posX = posX;
		this.posY = posY;
		this.width = width;
		this.height = height;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPosX(double posX) {
		this.posX = posX;
	}

	public void setPosY(double posY) {
		this.posY = posY;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	@Override
	public String toString() {
		return "Tank [Name="+ name + ", posX=" + posX + ", posY=" + posY + ", width=" + width + ", height=" + height + "]";
	}

	public double getPosX() {
		return posX;
	}

	public double getPosY() {
		return posY;
	}

	public double getWidth() {
		return width;
	}

	public double getHeight() {
		return height;
	}
	
	public String getName() {
		return name;
	}
	
	public Rectangle2D getBound() {
		return new Rectangle2D(posX, posY, width, height);
	}

}
