package level;

public enum Levels {
	
	LEVEL_1("level1"), LEVEL_2("level2");
	
	private String levelName;
	
	private Levels(String levelName) {
		this.levelName = levelName;
	}
	
	public String getName() {
		return this.levelName;
	}

}
