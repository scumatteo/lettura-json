package level;

public class Projectile {
	
	private double posX;
	private double posY;
	private double ray;

	public Projectile(double posX, double posY, double ray) {
		super();
		this.posX = posX;
		this.posY = posY;
		this.ray = ray;
	}
}
