package level;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class Level {
	World world = new World();
	private Levels level;
	private String path = "resources/level/";
	private String extension = ".json";
	private Save save = new Save();
	private Map<String, List<Double>> tank = new HashMap<>();
	
	public void loadLevel() throws FileNotFoundException, IOException {
		
		JSONParser json= new JSONParser();
		
      
        	try {
				Object o = json.parse(new FileReader(path + level.getName() + extension));
				JSONObject jo = (JSONObject) o;
				JSONArray player = (JSONArray) jo.get("playerTank");
				JSONArray enemy = (JSONArray) jo.get("enemyTank");
				tank.put("playerTank", new ArrayList<>());
				tank.put("enemyTank", new ArrayList<>());
				for(Object i : player) {
					if(i instanceof Double) {
						tank.get("playerTank").add((Double)i);
					}
				}
				for(Object i : enemy) {
					if(i instanceof Double) {
						tank.get("enemyTank").add((Double)i);
					}
				}
				world.setLevelWorld(tank);
				//save.save(this.world.getPlayerTank(), this.world.getEnemyTank());
					System.out.println(level.getName());
				System.out.println(this.world.getPlayerTank().toString());
				System.out.println(this.world.getEnemyTank().toString());
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	

        	if(this.world.getPlayerTank().getBound().intersects(this.world.getEnemyTank().getBound())) {
        		System.out.println("collision");
        	}
        
	}
	public void setLevel(Levels level) {
		this.level = level;
	}

}
