package level;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;

public class Loader {
	private Levels currentLevel;
	private Iterator<Levels> levels = Arrays.asList(Levels.values()).iterator();
	Level lev = new Level();
	
	public void loader() throws FileNotFoundException, IOException {
		this.currentLevel = levels.next();
		lev.setLevel(currentLevel);
		lev.loadLevel();
		this.currentLevel = levels.next();
		lev.setLevel(currentLevel);
		lev.loadLevel();
	}

}
