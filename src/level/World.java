package level;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class World {
	
	private Tank playerTank = new Tank("playerTank");
	private Tank enemyTank = new Tank("enemyTank");
	
	public void setLevelWorld(Map<String, List<Double>> objects) {
		Iterator<Double> init = objects.get(playerTank.getName()).iterator();
		this.playerTank.setTank(this.setField(init), this.setField(init), this.setField(init), this.setField(init));
		init = objects.get(enemyTank.getName()).iterator();
		this.enemyTank.setTank(this.setField(init), this.setField(init), this.setField(init), this.setField(init));
	}
	
	private double setField(Iterator<Double> i) {
		return i.hasNext() ? i.next() : 0;
	}
	
	public Tank getPlayerTank() {
		return playerTank;
	}
	public Tank getEnemyTank() {
		return enemyTank;
	}

	
	

}
